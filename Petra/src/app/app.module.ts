import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './components/app-component/app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { AsideMenuComponent } from './components/aside-menu/aside-menu.component';
import { DropDownDirective } from './core/directives/drop-down.directive';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { MenuHoverDirective } from './core/directives/menu-hover.directive';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BaseInterceptor } from './core/interceptors/base-interceptor.interceptor';
import { ModalBodyContainerDirective } from './core/directives/modal-body-container.directive';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AsideMenuComponent,
    DropDownDirective,
    MenuHoverDirective,
    DashboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: BaseInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
