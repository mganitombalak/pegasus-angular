import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { AuthService } from 'src/app/modules/auth/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, AfterViewInit, OnDestroy {

  private onAuthChangeSubs: Subscription;
  buttonText = 'Sign In';
  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.onAuthChangeSubs = this.authService.onAuthStatusChanged.subscribe(r => {
      if (r) {
        this.buttonText = 'Sign Out';
      } else {
        this.buttonText = 'Sign In';
      }
    });
   }

  ngAfterViewInit(): void { }

  ngOnDestroy(): void {
    this.onAuthChangeSubs.unsubscribe();
  }
  onLoginLogout() {
    this.authService.logout();
  }

}
