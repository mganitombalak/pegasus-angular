import { Component, OnInit } from '@angular/core';
import { BaseModalComponent } from 'src/app/core/base/base-modal-component';
import { ICategory } from 'src/app/core/models/ICategory';
import { CategoryService } from '../../services/category.service';
import { ComponentMode } from 'src/app/core/enum/component-mode';
import { JsHelper } from 'src/app/core/helper/to-form-data';

@Component({
  selector: 'app-category-detail',
  templateUrl: './category-detail.component.html',
  styleUrls: ['./category-detail.component.css']
})
export class CategoryDetailComponent extends BaseModalComponent<ICategory> implements OnInit {

  constructor(private categoryService: CategoryService) {
    super();
  }

  ngOnInit(): void {
  }

  onFormSubmit() {
    switch (this.componentMode) {
      case ComponentMode.Edit:
        const data = JsHelper.toFormData(this.model);
        data.set('iconFile', null);
        this.categoryService.update(data).subscribe(r => console.log('updated'));
        break;
    }
  }

}
