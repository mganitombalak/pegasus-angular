import { Component, OnInit, AfterViewInit } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { ICategory } from 'src/app/core/models/ICategory';
import { take } from 'rxjs/operators';
import { GridActionCellComponent } from 'src/app/modules/shared/components/grid-action-cell/grid-action-cell.component';
import { ModalService } from 'src/app/modules/shared/services/modal.service';
import { CategoryDetailComponent } from '../category-detail/category-detail.component';
import { ComponentMode } from 'src/app/core/enum/component-mode';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.css']
})
export class CategoryListComponent implements OnInit, AfterViewInit {

  columnDefs = [
    { headerName: '#', field: 'displayOrder' },
    { headerName: 'Name', field: 'name' },
    { headerName: 'Status', field: 'isActive' },
    { headerName: 'Created At', field: 'createdAt' },
    { headerName: 'Action', width: 250, cellRendererFramework: GridActionCellComponent },
  ];
  model: Array<ICategory>;
  context: any;
  boyunuAyarla() {
    return 38;
  }

  constructor(private service: CategoryService, private modalService: ModalService) { }

  ngOnInit() { this.context = { parent: this }; }

  ngAfterViewInit(): void {
    this.service.findAll().pipe(take(1)).subscribe(result => this.model = result.data);
  }

  onEdit(data: ICategory) {
    this.modalService.open({
      title: `Edit ${data.name}`,
      activeComponent: CategoryDetailComponent,
      componentMode: ComponentMode.Edit,
      data
    });
    console.log(`CLC:${data.name} is editing.`);
  }

  onDelete(data: ICategory) {
    this.modalService.open({
      title: `Delete ${data.name}`,
      activeComponent: CategoryDetailComponent,
      componentMode: ComponentMode.Delete,
      data
    });
    console.log(`CLC:${data.name} is deleting.`);
  }

  onInfo(data: ICategory) {
    console.log(`CLC:${data.name} is showing.`);
  }

}
