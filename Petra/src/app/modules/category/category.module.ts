import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryListComponent } from './components/category-list/category-list.component';
import { AgGridModule } from 'ag-grid-angular';
import { CategoryService } from './services/category.service';
import { SharedModule } from '../shared/shared.module';
import { GridActionCellComponent } from '../shared/components/grid-action-cell/grid-action-cell.component';
import { CategoryDetailComponent } from './components/category-detail/category-detail.component';

@NgModule({
  declarations: [CategoryListComponent, CategoryDetailComponent],
  imports: [
    SharedModule,
    FormsModule,
    CategoryRoutingModule,
    AgGridModule.withComponents([GridActionCellComponent])
  ],
  providers: [CategoryService]
})
export class CategoryModule { }
