import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, ComponentFactoryResolver } from '@angular/core';
import { ModalBodyContainerDirective } from 'src/app/core/directives/modal-body-container.directive';
import { ModalService } from '../../services/modal.service';
import { BaseModalComponent } from 'src/app/core/base/base-modal-component';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.css']
})
export class ModalComponent implements OnInit, AfterViewInit {

  @ViewChild('modalContainer', { static: false }) modalContainer: ElementRef;
  @ViewChild(ModalBodyContainerDirective, { static: false }) modalBodyContainer: ModalBodyContainerDirective;

  constructor(public modalService: ModalService, private componentFactoryResolver: ComponentFactoryResolver) { }

  ngOnInit(): void {
  }

  ngAfterViewInit(): void {
    this.modalService.setup({ modalComponent: this, modalContainer: this.modalContainer });
  }

  onOpening(): void {
    if (this.modalBodyContainer) {
      const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.modalService.modalOptions.activeComponent);
      const componentRef = this.modalBodyContainer.element.createComponent(componentFactory);
      (componentRef.instance as BaseModalComponent<any>).bind({
        data: this.modalService.modalOptions.data,
        componentMode: this.modalService.modalOptions.componentMode
      });
    }
  }
  onClosing(): void {
    this.modalBodyContainer.element.clear();
  }
}
