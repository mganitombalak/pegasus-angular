import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GridActionCellComponent } from './grid-action-cell.component';

describe('GridActionCellComponent', () => {
  let component: GridActionCellComponent;
  let fixture: ComponentFixture<GridActionCellComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GridActionCellComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GridActionCellComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
