import { Injectable, RendererFactory2, Renderer2 } from '@angular/core';
import { IModalSetupOptions } from 'src/app/core/models/IModalSetupOptions';
import { IModalOptions } from 'src/app/core/models/IModalOptions';

@Injectable({ providedIn: 'root' })
export class ModalService {
  modalSetupOptions: IModalSetupOptions = {};
  modalOptions: IModalOptions;
  private renderer: Renderer2;
  constructor(private rendererFactory: RendererFactory2) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  setup(setupOptions: IModalSetupOptions): void {
    this.modalSetupOptions.modalComponent = setupOptions.modalComponent;
    this.modalSetupOptions.modalContainer = setupOptions.modalContainer;
  }

  open(options: IModalOptions): void {
    this.modalOptions = options;
    this.modalSetupOptions.modalComponent.onOpening();
    this.renderer.setStyle(this.modalSetupOptions.modalContainer.nativeElement, 'display', 'block');
    setTimeout(() => this.renderer.addClass(this.modalSetupOptions.modalContainer.nativeElement, 'show'), 100);
  }

  close(): void {
    this.modalSetupOptions.modalComponent.onClosing();
    this.renderer.setStyle(this.modalSetupOptions.modalContainer.nativeElement, 'display', 'none');
    setTimeout(() => this.renderer.addClass(this.modalSetupOptions.modalContainer.nativeElement, 'hide'), 100);
  }
}
