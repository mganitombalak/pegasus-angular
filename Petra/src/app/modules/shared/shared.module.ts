import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridActionCellComponent } from './components/grid-action-cell/grid-action-cell.component';
import { ModalComponent } from './components/modal/modal.component';
import { ModalBodyContainerDirective } from 'src/app/core/directives/modal-body-container.directive';



@NgModule({
  declarations: [GridActionCellComponent, ModalComponent, ModalBodyContainerDirective],
  imports: [
    CommonModule
  ],
  exports: [CommonModule, GridActionCellComponent, ModalComponent]
})
export class SharedModule { }
