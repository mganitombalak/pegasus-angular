import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { IAuthRequest } from 'src/app/core/models/IAuthRequest';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: IAuthRequest = { username: '', password: '' };
  constructor(private authService: AuthService) { }

  ngOnInit(): void { }

  onSubmit() {
    this.authService.login(this.model);
  }



}
