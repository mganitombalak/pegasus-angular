import { Type } from '@angular/core';
import { ComponentMode } from '../enum/component-mode';

export interface IModalOptions {
    title?: string;
    componentMode: ComponentMode;
    activeComponent?: Type<any>;
    data?: any;
}
