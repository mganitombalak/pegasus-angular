export interface ICategory {
    id: string;
    name: string;
    displayOrder: number;
    iconFile: string;
    iconPath: string;
    iconFileContentType: string;
    isActive: boolean;
    createdAt: string;
}