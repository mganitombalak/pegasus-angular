export interface IBaseResponse<T> {
    totalRecordCount: number;
    data: Array<T>;
    humanReadableMessage: Array<string>;
}
