export interface IMenuItem {
    title: string;
    displayOrder: number;
    createdAt: string;
    isActive: boolean;
}
