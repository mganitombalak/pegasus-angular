import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { IBaseResponse } from '../models/IBaseReponse';

export class BaseCrudService<T> {

    protected endPoint: string;
    constructor(protected httpClient: HttpClient) { }
    findAll(): Observable<IBaseResponse<T>> {
        return this.httpClient.get(this.endPoint) as Observable<IBaseResponse<T>>;
    }

    update(model: T | FormData): Observable<IBaseResponse<T>> {
        return this.httpClient.put(this.endPoint, model) as Observable<IBaseResponse<T>>;
    }
}
