import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class BaseInterceptor implements HttpInterceptor {

  constructor() { }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    let requestObject = request.clone({
      url: `${environment.baseUrl + request.url}`,
      headers: request.headers
        .set('vendor', 'Pegasus')
        .set('Access-Control-Allow-Origin', '*')
        .set('Authorization', `Bearer ${localStorage.getItem('token')}`)
    });
    if (!(request.body instanceof FormData)) {
      requestObject = requestObject.clone({ headers: requestObject.headers.append('Content-Type', 'application/json') });
    }
    return next.handle(requestObject);
  }
}
