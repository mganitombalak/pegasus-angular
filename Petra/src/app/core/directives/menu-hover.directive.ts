import { Directive, ElementRef, Renderer2, HostListener } from '@angular/core';

@Directive({
  selector: '[appMenuHover]'
})
export class MenuHoverDirective {

  constructor(private element: ElementRef, private renderer: Renderer2) { }

  @HostListener('mouseenter', ['$event']) OnMouseEnter(event: Event) {
    this.renderer.addClass(this.element.nativeElement, 'active');
  }
  @HostListener('mouseleave', ['$event']) OnMouseLeave(event: Event) {
    this.renderer.removeClass(this.element.nativeElement, 'active');
  }

}
