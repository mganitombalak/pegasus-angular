import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({ selector: '[appDropDown]' })
export class DropDownDirective{

  private SubContainer: any;
  constructor(private element: ElementRef, private renderer: Renderer2) {
    // console.log(element.nativeElement.id);
  }

  @HostListener('mouseenter', ['$event']) OnMouseEnter(event: Event) {
    // console.log(this.element.nativeElement.id + ' mouse entered.');
    this.SubContainer = this.element.nativeElement.querySelector('.dropdown-menu');
    this.renderer.addClass(this.SubContainer, 'd-block');
    this.renderer.removeClass(this.SubContainer, 'd-none');
  }

  @HostListener('mouseleave', ['$event']) OnMouseLeave(event: Event) {
    this.SubContainer = this.element.nativeElement.querySelector('.dropdown-menu');
    this.renderer.addClass(this.SubContainer, 'd-none');
    this.renderer.removeClass(this.SubContainer, 'd-block');
  }

}
