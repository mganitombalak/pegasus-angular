import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IMenuItem } from 'src/app/core/models/IMenuItem';
import { BaseCrudService } from 'src/app/core/base/base-crud-service';

@Injectable({ providedIn: 'root' })
export class MenuService extends BaseCrudService<IMenuItem> {
  constructor(protected httpClient: HttpClient) {
    super(httpClient);
    this.endPoint = 'menu';
  }
}
