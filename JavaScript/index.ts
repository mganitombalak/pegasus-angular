interface IPerson {
    getFullName(): string;
}

class Person implements IPerson {
    constructor(
        private name: string,
        private surname: string) {
    }
    // name: string;
    // surname: string;
    getFullName(): string {
        return this.name + " " + this.surname;
    }
}

class Greeting {
    sayHalo(p: IPerson): void {
        console.log(
            "Halo, Ich bin "
            // + p.name + " " + p.surname
            + p.getFullName());
    }
}
let p = new Person("Gani", "Tombalak");
let g = new Greeting();
g.sayHalo(p);