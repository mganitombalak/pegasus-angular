// Literal Object
var obj = { 
    name: 'Gani', 
    surname: 'Tombalak',
    getFullName:function(){
        console.log("==============getFullName Context================");
        console.log(this);
        console.log("=================================================");
        return this.name + " " + this.surname;
    } 
};

console.log(obj.name);
console.log(obj.surname);
console.log('=========================================================')
console.log(obj.getFullName);
console.log(obj.getFullName());