function Person(param_name, param_surname) {
    this.name = param_name;
    this.surname = param_surname;
}
var p = new Person("Gani", "Tombalak");
var p2 = new Person("Adem", "Korkmaz");
console.log(p.name);
console.log(p.surname);
console.log("=================================================");
console.log(p2.name);
console.log(p2.surname);