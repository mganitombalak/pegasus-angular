var Person = /** @class */ (function () {
    function Person(name, surname) {
        this.name = name;
        this.surname = surname;
    }
    // name: string;
    // surname: string;
    Person.prototype.getFullName = function () {
        return this.name + " " + this.surname;
    };
    return Person;
}());
var Greeting = /** @class */ (function () {
    function Greeting() {
    }
    Greeting.prototype.sayHalo = function (p) {
        console.log("Halo, Ich bin "
            // + p.name + " " + p.surname
            + p.getFullName());
    };
    return Greeting;
}());
var p = new Person("Gani", "Tombalak");
var g = new Greeting();
g.sayHalo(p);
